#! /bin/bash
download_youtube(){
    (
    cd $HOME/Videos
    you-get -s 127.0.0.1:1081  "$1"
    result=$?
    while [[ $result != 0 ]]; do
        you-get -s 127.0.0.1:1081  "$1"
        result=$?
    done
    )
}

if [[ $1 ]]; then
    list_file=$1
else
    list_file="waiting_list.txt"
fi

while read LINE; do
    download_youtube $LINE
    if [[ $? == 0 ]]; then
        # the following <fw-slash> was beacause
        # '/' is a special character for sed
        b=$(echo $LINE | sed -e 's/\//<fw-slash>/g')
        sed -i 's/\//<fw-slash>/g' "$list_file"
        sed -i "/^$b$/d" "$list_file"
        sed -i 's/<fw-slash>/\//g' "$list_file"
    fi
done < "$list_file"
